
// Ryan Appel
// Playing Cards

#include <iostream>
#include <conio.h>

using namespace std;

// enums for rank & suit
enum Rank
{
	Two = 2, Three, Four, Five, Six, Seven,
	Eight, Nine, Ten, Jack, Queen, King, Ace
};

enum Suit
{
	Spades, Hearts, Clubs, Diamonds
};


// struct for card
struct Card
{
	Rank rank;
	Suit suit;
};


void PrintCard(Card card);

Card HighCard(Card card1, Card card2);

int main()
{
	Card a;
	a.rank = Jack;
	a.suit = Hearts;

	Card b;
	b.rank = Ace;
	b.suit = Spades;

	Card c;
	c.rank = Ace;
	c.suit = Diamonds;

	Card d;
	d.rank = Three;
	d.suit = Diamonds;

  /*try {  //suggested usage
		PrintCard(d);
		HighCard(b, c);
	}
	catch (const char* msg){
		cerr << msg << endl;
  }*/

	(void)_getch();
	return 0;
}

void PrintCard(Card card)
{
	string tempRank;
	string tempSuit;

	switch (card.rank)
	{
	case 2:
		tempRank = "Two";
		break;
	case 3:
		tempRank = "Three";
		break;
	case 4:
		tempRank = "Four";
		break;
	case 5:
		tempRank = "Five";
		break;
	case 6:
		tempRank = "Six";
		break;
	case 7:
		tempRank = "Seven";
		break;
	case 8:
		tempRank = "Eight";
		break;
	case 9:
		tempRank = "Nine";
		break;
	case 10:
		tempRank = "Ten";
		break;
	case 11:
		tempRank = "Jack";
		break;
	case 12:
		tempRank = "Queen";
		break;
	case 13:
		tempRank = "King";
		break;
	case 14:
		tempRank = "Ace";
		break;		
	}

	switch (card.suit)
	{
	case 0:
		tempSuit = "Spades";
		break;
	case 1:
		tempSuit = "Hearts";
		break;
	case 2:
		tempSuit = "Clubs";
		break;
	case 3:
		tempSuit = "Diamonds";
		break;
	}

	std::cout << "The " << tempRank << " of " << tempSuit << endl;
}

Card HighCard(Card card1, Card card2)
{
		if (card1.rank > card2.rank)
			return card1;
		else if (card1.rank < card2.rank)
			return card2;
//		else  //added because why not?
//			throw "Error: Card ranks are equal!";
}
